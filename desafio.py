# -*- coding: utf-8 -*-
"""
Created on Fri Nov 18 18:21:21 2016

@author: tacio
"""

import gzip
import json
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

from collections import OrderedDict

import pickle

folder = "C:\\Users\\tacio\\Documents\\codigo\\chaordic\\"
inputfile = folder + "user-product_map.json.gz"

# abre file handler
f = gzip.open(inputfile, 'r')

data = []
garbage = []

# lê linha a linha, retorno do gzip vem em formato binário
for l in f:

    # decodifica, convertendo para str
    ls = l.decode()
    
    # desserializa o json para dict
    try:
        d = json.loads(ls)
        data.append(d)
    except json.JSONDecodeError:
        print("linha ruim: {}".format(ls))
        garbage.append(ls)

f.close()
        
# len(lixo)/(len(lixo)+len(dados))

# Converte em Pandas Dataframe para análise exploratórias        
df = pd.DataFrame(data)

# contagem de produtos distintos
nprods = df.product_id.nunique()

# histograma de produtos
hist_prod = df.product_id.value_counts()
(hist_prod.value_counts()/nprods).head()

# contagem de visitantes únicos
nusers = df.user_id.nunique()

# histograma de produtos
hist_user = df.user_id.value_counts()
(hist_user.value_counts()/nusers).head()

def make_pairs(ls):
    size = len(ls)
    pairs = []
    for i in range(size):
        for j in range(i+1,size):
            pairs.append((ls[i], ls[j]))
    
    return pairs


# poderia usar um defaultdict, sintaxe fica mais sucinta mas menos explícita
prod_pairs = {}

for v in hist_user.keys():
    if (hist_user[v] > 1):
        prods_v = list(set([x['product_id'] for x in data if x['user_id'] == v and hist_prod[x['product_id']] > 1]))
        prods_v.sort()
        
        prod_pairs_v = make_pairs(prods_v)
        for pp in prod_pairs_v:
            if pp not in prod_pairs:
                prod_pairs[pp] = 0
            
            prod_pairs[pp] += 1

pickle.dump(prod_pairs, open(folder+"prod_pairs.pkl",'wb'))
prod_pairs = pickle.load(open(folder+"prod_pairs.pkl",'rb'))

# calcula jaccard
recs = {}
i=0
for p in hist_prod.keys():
    if (hist_prod[p] > 1):
        prod_pairs_p = {pp[1]: prod_pairs[pp]/(hist_prod[p] + hist_prod[pp[1]] - prod_pairs[pp]) for pp in prod_pairs if pp[0] == p}
        prod_pairs_p.update({pp[0]: prod_pairs[pp]/(hist_prod[p]+hist_prod[pp[0]] - prod_pairs[pp]) for pp in prod_pairs if pp[1] == p})
        
        # tira potenciais recomendações com similaridade menor que 0.005
        prod_pairs_p = {prec:float(round(prod_pairs_p[prec],2)) for prec in prod_pairs_p if prod_pairs_p[prec] > 5e-3}

        # ordena por similaridade e guarda apenas as 40 maiores        
        recs_p = sorted(prod_pairs_p.items(), key=lambda _:-_[1])[:40]
        
        if (len(recs_p) > 0):
            i+=1
            recs[p] = sorted(prod_pairs_p.items(), key=lambda _:-_[1])[:40]
        
            if(i%100 == 0):
                pickle.dump(recs, open(folder+"recs.pkl",'wb'))

pickle.dump(recs, open(folder+"recs.pkl",'wb'))


# tem recomendações repetidas?
for p in recs:
    recs_p = [x[0] for x in recs[p]]
    if (len(recs_p) > len(set(recs_p))):
        print("{} tem recomendações repetidas!".format(p))            


with open(folder+"itemwise_jaccard_sim.json",'w') as recs_file:
    for p in recs:
        r = OrderedDict([("reference_product_id", int(p)), \
            ("recommendations", [OrderedDict([("product_id", int(prec[0])), \
                ("similarity", float(round(prec[1],2)))]) for prec in recs[p]])])
            
        recs_file.write(json.dumps(r)+"\n")
    



